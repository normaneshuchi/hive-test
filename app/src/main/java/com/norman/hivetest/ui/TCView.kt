package com.norman.hivetest.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider

import com.norman.hivetest.R
import kotlinx.android.synthetic.main.t_c_view_fragment.*

class TCView : Fragment() {

    companion object {
        fun newInstance() = TCView()
    }

    private lateinit var viewModel: TCViewViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.t_c_view_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(TCViewViewModel::class.java)
//
//
//        tcWebview.loadUrl("https://jerseystore.co.ke/terms")
//        tcWebview.show
    }

}
