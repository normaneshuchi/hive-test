package com.norman.hivetest.ui

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.util.Base64
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.norman.hivetest.MainActivity

import com.norman.hivetest.R
import kotlinx.android.synthetic.main.profile_fragment.*


class Profile : Fragment() {

    companion object {
        fun newInstance() = Profile()
    }

    private lateinit var viewModel: ProfileViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.profile_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(ProfileViewModel::class.java)

        val name = "Samuel L. Jackson"

        val first = name.take(1)
        profile_image.visibility = GONE
        profileText.text = first
        pdCard.setOnClickListener {
            findNavController().navigate(R.id.profile_to_details)
        }

        termsCard.setOnClickListener {
//            findNavController().navigate(R.id.profile_to_tc)
            (activity as MainActivity).openBrowserActivity()
        }
    }

    fun openTC() {

    }

}
