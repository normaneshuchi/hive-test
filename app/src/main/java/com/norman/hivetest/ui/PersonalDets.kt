package com.norman.hivetest.ui

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController

import com.norman.hivetest.R
import kotlinx.android.synthetic.main.personal_dets_fragment.*
import kotlinx.android.synthetic.main.profile_fragment.*
import kotlinx.android.synthetic.main.profile_fragment.backText
import kotlinx.android.synthetic.main.profile_fragment.profileText

class PersonalDets : Fragment() {

    companion object {
        fun newInstance() = PersonalDets()
    }

    private lateinit var viewModel: PersonalDetsViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.personal_dets_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(PersonalDetsViewModel::class.java)

        profileText.visibility = GONE

        backText.setOnClickListener {
            findNavController().navigateUp()
        }
    }

}
