package com.norman.hivetest

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.browser.customtabs.CustomTabsIntent
import saschpe.android.customtabs.CustomTabsHelper
import saschpe.android.customtabs.WebViewFallback


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun openBrowserActivity() {
//        val openURL = Intent(Intent.ACTION_VIEW)
//        openURL.data = Uri.parse("https://developer.android.com/studio/terms")
//        startActivity(openURL)

        val customTabsIntent = CustomTabsIntent.Builder()
            .addDefaultShareMenuItem()
            .setToolbarColor(
                this.resources.getColor(R.color.colorPrimary)
            )
            .setShowTitle(true)
//            .setCloseButtonIcon(resources.getDrawable(R.drawable.ic_back))
            .build()

        CustomTabsHelper.addKeepAliveExtra(this, customTabsIntent.intent)

        CustomTabsHelper.openCustomTab(
            this, customTabsIntent,
            Uri.parse("https://developer.android.com/studio/terms"),
            WebViewFallback()
        )
    }


}
